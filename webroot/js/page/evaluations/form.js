/**
 * いいね登録/削除
 */

$( function() {
    // 非同期でいいねを付与/取消する
    $('.ajax-btn').click(function() {
        $(this).toggleClass('on');
        var status = $(this).attr('id');
        var inputElement = $(this);
        setEvaluationStatus(status, inputElement);

        var postId = $(this).next('#ajax-post').val();
        var hostUrl= '/evaluations/post';

        $.ajax({
            url: hostUrl,
            type:'POST',
            dataType: 'json',
            data : {post_id : postId, status : status},
            timeout:3000,
        }).done(function(response) {
           
        }).fail(function(error) {
            var status = inputElement.attr('id');
            setEvaluationStatus(status, inputElement);
        })

        function setEvaluationStatus(status, inputElement) {
            if (status === '0') {
                inputElement.attr('id', '1');
                inputElement.attr('value', 'いいね');
            } else {
                inputElement.attr('id', '0');
                inputElement.attr('value', 'いいねを取り消す');
            }
        }
    });
} );