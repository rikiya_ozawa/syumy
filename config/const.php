<?php
/**
 * 定数定義
 */

// ユーザーステータスが有効
define("USER_STATUS_ACTIVE", 1);

// 投稿ステータスが有効
define("POST_STATUS_ACTIVE", 1);
// 投稿ステータスが無効
define("POST_STATUS_DELETE", 0);

// いいねステータスが有効
define("EVALUATION_STATUS_ACTIVE", 1);
// いいねステータスが無効
define("EVALUATION_STATUS_DELETE", 0);

// エラーメッセージ
define('ERR_MSG_400',            '予期していないアクセスです');
define('ERR_MSG_403',            '許可されていないアクセスです');
define('ERR_MSG_403_NEED_LOGIN', 'ログインする必要があります');
define('ERR_MSG_404',            'お探しのページは削除された可能性があります');
define('ERR_MSG_500',            'システムエラーが発生しました');
define('ERR_MSG_OTHER',          '予期せぬエラーが発生しました');