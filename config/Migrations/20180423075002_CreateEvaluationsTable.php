<?php
use Migrations\AbstractMigration;

class CreateEvaluationsTable extends AbstractMigration
{
    /**
     * up
     * 
     * @return void
     */
    public function up(){
        $this->execute("
	    CREATE TABLE `evaluations` (
	      `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '評価ID',
          `user_id` int(10) unsigned NOT NULL COMMENT 'ユーザーID',
          `post_id` int(10) unsigned NOT NULL COMMENT '投稿ID',
          `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'レコードステータス',
	      `created` datetime DEFAULT NULL COMMENT '作成日時',
	      `modified` datetime DEFAULT NULL COMMENT '更新日時',
          PRIMARY KEY (`id`),
          FOREIGN KEY (`user_id`)
            REFERENCES users(`id`),
          FOREIGN KEY (`post_id`)
            REFERENCES posts(`id`),
	      KEY `status` (`status`),
          KEY `user_id` (`user_id`),
          KEY `post_id` (`post_id`)
	    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

	    SET FOREIGN_KEY_CHECKS = 1;
	");
    }

    /**
     * down
     *
     * @return void
     */
    public function down(){
       $this->execute("
           DROP TABLE IF EXISTS `evaluations`;
       "); 
    }
}
