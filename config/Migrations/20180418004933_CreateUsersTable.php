<?php
use Migrations\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{
    /**
     * up
     * 
     * @return void
     */
    public function up(){
        $this->execute("
	    CREATE TABLE `users` (
	      `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ユーザーID',
	      `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'レコードステータス',
	      `name` varchar(20) COLLATE utf8mb4_bin NOT NULL COMMENT 'ニックネーム',
	      `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'メールアドレス',
	      `password` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'パスワード',
	      `image` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '画像パス',
	      `created` datetime DEFAULT NULL COMMENT '作成日時',
	      `modified` datetime DEFAULT NULL COMMENT '更新日時',
	      PRIMARY KEY (`id`),
	      KEY `status` (`status`),
          KEY `name` (`name`),
          KEY `email` (`email`)
	    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

	    SET FOREIGN_KEY_CHECKS = 1;
	");
    }

    /**
     * down
     *
     * @return void
     */
    public function down(){
       $this->execute("
           DROP TABLE IF EXISTS `users`;
       "); 
    }
}
