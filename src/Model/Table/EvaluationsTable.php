<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Entity\Evaluation;
use Cake\Core\Configure;

class EvaluationsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo('Users')
            ->setForeignKey('user_id')
            ->setJoinType('INNER');
        $this->belongsTo('Posts')
            ->setForeignKey('post_id')
            ->setJoinType('INNER');
 
        $this->setTable('Evaluations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
 
        $this->addBehavior('Timestamp');
    }

    /**
     * バリデーション
     * 
     * @param Validator $validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('post_id')
            ->requirePresence('post_id')
            ->notEmpty('post_id', '投稿IDを指定してください');

        $validator
            ->scalar('user_id')
            ->requirePresence('user_id')
            ->notEmpty('user_id', 'ユーザIDを指定してください');

        return $validator;
    }

    /**
     * 評価エンティティ生成
     * 
     * @param array $requestData リクエストデータ
     * @return \App\Model\Entity\Evaluation
     */
    public function createEvaluationEntity(array $requestData)
    {
        // 投稿保存エンティティ
        $saveEvaluation = $this->newEntity();

        // 結合
        $saveEvaluation = $this->patchEntity(
            $saveEvaluation,
            $requestData,
            ['validate' => 'default']
        );

        $evaluation = $this->find()
            ->where([
                'Evaluations.user_id' => $requestData['user_id'],
                'Evaluations.post_id' => $requestData['post_id'],
            ])
            ->first();

        // 既に存在するとき
        if (! empty($evaluation)) {
            $saveEvaluation->isNew(false);
            $saveEvaluation->set('id', $evaluation->id);
        }

        return $saveEvaluation;
    }

    /**
     * 投稿保存
     * 
     * @param Evaluation $evaluation 投稿エンティティ
     * @return \App\Model\Entity\Evaluation
     */
    public function saveEvaluation(Evaluation $evaluation)
    {
        // バリデーション
        if ($evaluation->getErrors()) {
            return false;
        }
        
        return $this->save($evaluation);
    }
}
