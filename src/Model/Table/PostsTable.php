<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Entity\Post;
use Cake\Core\Configure;

class PostsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo('Users')
            ->setForeignKey('user_id')
            ->setJoinType('INNER');

        $this->hasMany('Evaluations', [
                'className' => 'Evaluations',
            ])
            ->setForeignKey('post_id');
 
        $this->setTable('posts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
 
        $this->addBehavior('Timestamp');
    }

    /**
     * バリデーション
     * 
     * @param Validator $validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
 
        return $validator;
    }

    /**
     * 投稿登録バリデーション
     * 
     * @param Cake\Validation\Validator $validator
     */
    public function validationAddPost(Validator $validator)
    {
        // defaultの検証内容を引き継ぐ
        $validator = $this->validationDefault($validator);

        $validator
            ->scalar('content')
            ->requirePresence('content', 'create')
            ->notEmpty('content', 'つぶやきが入力されていません');

        return $validator;
    }

    /**
     * 投稿エンティティ生成
     * 
     * @param array $requestData リクエストデータ
     * @param string $type: 'edit', 'add'
     * @return \App\Model\Entity\Post
     */
    public function createPostEntity(array $requestData, string $type)
    {
        // 投稿保存エンティティ
        $savePost = $this->newEntity();

        // 編集のとき
        if ($type === 'edit' || $type === 'delete') {
            $savePost->isNew(false);
            if (isset($requestData['id'])) {
                $savePost->set('id', $requestData['id']);
            }
        }
        
        // 削除のとき
        if ($type === 'delete') {
            $savePost->isNew(false);
            $savePost->set('status', POST_STATUS_DELETE);
        }

        // 結合
        $savePost = $this->patchEntity(
            $savePost,
            $requestData,
            ['validate' => 'addPost']
        );

        return $savePost;
    }

    /**
     * 投稿保存
     * 
     * @param User $user 投稿エンティティ
     * @return \App\Model\Entity\Post
     */
    public function savePost(Post $post)
    {
        // バリデーション
        if ($post->getErrors()) {
            return false;
        }

        return $this->save($post);
    }

    /**
     * 投稿一覧取得
     * 
     * @param array|null $option 検索オプション
     * @return App\Model\Entity\Post
     */
    public function getPosts($option = null)
    {
        // 検索条件
        $where[] = [
            $this->getAlias() . '.status' => POST_STATUS_ACTIVE,
        ];

        // 投稿検索のとき
        if (! empty($option['search'])) {
            $where[] = [
                $this->getAlias() . '.content LIKE' => '%' . $option['search'] . '%',
            ];
        }

        $posts = $this->find()
            // Users
            ->contain('Users', function ($query){
                return $query
                    ->select(['id', 'name'])
                    ->where(['Users.status' => USER_STATUS_ACTIVE]);
            })
            // Evaluations
            ->contain('Evaluations', function ($query) {
                return $query
                    ->select(['id', 'user_id', 'post_id', 'status'])
                    ->where(['Evaluations.status' => EVALUATION_STATUS_ACTIVE]);
            })
            ->where($where)
            ->order(['Posts.modified' =>'DESC'])
            ->all();
            
        return $posts;
    }

    /**
     * ログインユーザーがいいねした投稿を取得
     * 
     * @param int $authUserId ログインユーザーID
     * @return \App\Model\Entity\Post
     */
    public function getEvaluationsPostId(int $authUserId)
    {
        $evaluations = $this->find()
            ->contain('Evaluations', function ($query) use($authUserId) {
                return $query
                    ->select(['id', 'post_id', 'status'])
                    ->where(['Evaluations.user_id' => $authUserId])
                    ->where(['Evaluations.status' => EVALUATION_STATUS_ACTIVE]);
            })
            ->select(['id'])
            ->where(['Posts.status' => POST_STATUS_ACTIVE])
            ->order(['Posts.modified' =>'DESC'])
            ->all();

        return $evaluations;
    }
}
