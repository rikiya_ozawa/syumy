<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Entity\User;
use Cake\Core\Configure;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->hasMany('Posts')
            ->setForeignKey('user_id');
        $this->hasMany('Evaluations')
            ->setForeignKey('evaluation_id');
 
        $this->setTable('Users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
 
        $this->addBehavior('Timestamp');
    }

    /**
     * バリデーション
     * 
     * @param Validator $validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
 
        $validator
            ->scalar('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email', 'メールアドレスは必ず入力してください');

        $validator
            ->scalar('password')
            ->requirePresence('password', 'create')
            ->notEmpty('password', 'パスワードは必ず入力してください');

        return $validator;
    }

    /**
     * ユーザログインバリデーション
     * 
     * @param Cake\Validation\Validator $validator
     */
    public function validationLogin(Validator $validator)
    {
        // defaultの検証内容を引き継ぐ
        $validator = $this->validationDefault($validator);

        return $validator;
    }

    /**
     * ユーザー保存バリデーション
     * 
     * @param Cake\Validation\Validator $validator
     */
    public function validationAdd(Validator $validator)
    {
        // defaultの検証内容を引き継ぐ
        $validator = $this->validationDefault($validator);

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name', 'ユーザー名は必ず入力してください')
            ->add(
                'name',
                'unique',
                    [
                        'rule' => 'validateUnique',
                        'provider' => 'table',
                        'message' => 'ユーザー名はすでに使われています'
                    ]
                );

        $validator
            ->scalar('email')
            ->add(
                'email',
                'unique',
                    [
                        'rule' => 'validateUnique',
                        'provider' => 'table',
                        'message' => 'メールアドレスはすでに使われています'
                    ]
                );

        $validator
            ->scalar('password')
            ->add('password',[
            'compareWith' => [
                'rule' => ['compareWith','password_confirm'],
                'message' => '確認用のパスワードと一致しません'
            ]
        ]);

        return $validator;
    }

    /**
     * ユーザー編集バリデーション
     * 
     * @param Cake\Validation\Validator $validator
     */
    public function validationEdit(Validator $validator)
    {
        // defaultの検証内容を引き継ぐ
        $validator = $this->validationDefault($validator);

        $validator
            ->scalar('password')
            ->add('password',[
            'compareWith' => [
                'rule' => ['compareWith','password_confirm'],
                'message' => '確認用のパスワードと一致しません'
            ]
        ]);

        return $validator;
    }

    /**
     * 認証
     *
     * @param User $user ユーザーエンティティ
     * @return \App\Model\Entity\User
     */
    public function authenticate(User $user)
    {
        // バリデーション
        if ($user->getErrors()) {
            return false;
        }

        // 検索条件
        $where = [
            'email'     => $user->get('email'),
            'status'    => USER_STATUS_ACTIVE,
            'password'  => $user->get('password'),
        ];

        // 認証
        $authUser = $this
            ->find()
            ->where($where)
            ->first();
        return $authUser;
    }

    /**
     * ユーザー保存
     * 
     * @param User $user ユーザーエンティティ
     * @return \App\Model\Entity\User
     */
    public function saveUser(User $user)
    {
        // バリデーション
        if ($user->getErrors()) {
            return false;
        }

        return $this->save($user);
    }

    /**
     * ユーザーを特定する
     * 
     * @param int $id
     * @return @return \App\Model\Entity\User
     */
    public function findUser(int $user_id)
    {
        return $this->find()->where(["id=" . $user_id])->first();
    }
}
