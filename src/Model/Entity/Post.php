<?php

namespace App\Model\Entity;

use Cake\Utility\Security;
use Cake\ORM\Entity;

class Post extends Entity
{

    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
