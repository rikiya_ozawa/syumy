<?php

namespace App\Model\Entity;

use Cake\Utility\Security;
use Cake\ORM\Entity;

class Evaluation extends Entity
{

    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
