<?php

namespace App\Model\Entity;

use Cake\Utility\Security;
use Cake\ORM\Entity;

class User extends Entity
{

    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * パスワードハッシュ化
     * 
     * @return Cake\Utility\Security
     */
    protected function _setPassword($password)
    {
        return Security::hash($password, 'sha256', true);
    }
}
