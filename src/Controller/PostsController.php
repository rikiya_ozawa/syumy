<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Event\Event;
use App\Model\Model\Posts;

class PostsController extends AppController
{

    /**
     * initialization hook method
     * 
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Users = TableRegistry::get('Posts');
        $this->loadComponent('App');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /**
     * 投稿一覧
     * 
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $params = $this->request->getQueryParams();

        $option = [];
        // 検索のとき
        if (! empty($params['content'])) {
            $option['search'] = $params['content'];
        }

        // 投稿一覧
        $posts = $this->Posts->getPosts($option)->toArray();

        // ログインユーザID
        $authUserId = $this->getAuthUser()->id;
        // ログインユーザがいいねした投稿ID
        $evaluationPosts = $this->Posts->getEvaluationsPostId($authUserId)->toArray();

        // 登録フォーム用投稿エンティティ
        $post = $this->Posts->newEntity();

        $this->set(compact('posts', 'post', 'evaluationPosts'));
    }

    /**
     * 投稿登録
     * 
     * @return \Cake\Network\Response|void
     */
    public function add()
    {
        $this->__save('add');
    }

    /**
     * 投稿編集
     * 
     * @param int $id ユーザーID
     * @return \Cake\Network\Response|void
     */
    public function edit(int $id)
    {
        $this->__save('edit', $id);

        $post = $this->Posts->get($id);
        $this->set('post', $post);
    }

    /**
     * 投稿保存
     * 
     * @param string $type: 'edit', 'add'
     * @param int|null $userId ユーザID
     * @return \Cake\Http\Response|null
     */
    private function __save(string $type, int $userId = null)
    {
        // TODO: セキュリティ対策
        if ($this->request->is(['patch', 'post', 'put'])) {
            $params = $this->request->getData();

            // 登録のとき
            if ($type === 'add') {
                // ユーザー情報をリクエストデータとマージ
                $params = $this->App->mergeRequestData(
                    $params,
                    $this->getAuthUser()->get('id')
                );
            }

            $post = $this->Posts->createPostEntity($params, $type);

            try {
                $savePost = $this->Posts->savePost($post);
            } catch(Exception $e) {
                new \Cake\Http\Exception\InternalErrorException($e);
                return;
            }

            if ($savePost) {
                $this->Flash->success(__('保存しました。'));
            } else {
                $this->Flash->error(__('もう一度入力してください。'));
            }
            $this->redirect(['action' => 'index']);
        }
    }

    /**
     * 投稿削除
     * 
     * @param int $id ユーザID
     * @return \Cake\Http\Response
     */
    public function delete(int $id)
    {
        $this->__save('delete', $id);
    }
}
