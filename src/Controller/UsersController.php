<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Event\Event;
use App\Model\Model\Users;

class UsersController extends AppController
{

    /**
     * initialization hook method
     * 
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Users = TableRegistry::get('Users');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'login']);
    }

    /**
     * ログイン
     * 
     * @return \Cake\Http\Response|null
     */
    public function login()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity(
                $user,
                $this->request->getData(),
                ['validate' => 'login']
            );

            $authUser = $this->Users->authenticate($user);
            if ($authUser) {
                $this->Auth->setUser($authUser);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('ユーザー名かパスワードが間違っています。もう一度入力してください。'));
        }
    }

    /**
     * ログアウト
     * 
     * @return \Cake\Http\Response|null
     */
    public function logout()
    {
        $this->Flash->success('ログアウトしました');
        return $this->redirect($this->Auth->logout());
    }

    public function index()
    {   
        // TODO: 後で消す
    }

    /**
     * ユーザー登録
     * 
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity(
                $user,
                $this->request->getData(),
                ['validate' => 'add']
            );

            try {
                $saveUser = $this->Users->saveUser($user);
            } catch(Exception $e) {
                new \Cake\Http\Exception\InternalErrorException($e);
                return;
            }

            if ($saveUser) {
                $this->Flash->success(__('ユーザー登録が完了しました。'));
                return $this->redirect(['action' => 'login']);
                $this->Flash->error(__('もう一度入力してください。'));
            }
        }

        $this->set('user', $user);
    }

    /**
     * ユーザー編集
     * 
     * @param int $id
     * @return \Cake\Network\Response|void
     */
    public function edit(int $id)
    {
        // TODO: ほかのユーザが編集できないようにする
        $user = $this->Users->get($id);
        if ($this->request->is(['patch', 'post', 'put'] )) {
            $user = $this->Users->patchEntity(
                $user,
                $this->request->getData(),
                ['validate' => 'edit']
            );

            try {
                $saveUser = $this->Users->saveUser($user);
            } catch(Exception $e) {
                new \Cake\Http\Exception\InternalErrorException($e);
                return;
            }

            if ($saveUser) {
                $this->Flash->success(__('ユーザー編集が完了しました。'));
                return $this->redirect(['action' => 'edit', $id]);
                $this->Flash->error(__('もう一度入力してください。'));
            }
        } else {
            // TODO: 削除する
            $user = $this->Users->findUser($id);
            if (! $user) {
                
                $exception = new Cake\Http\Exception\NotFoundException('404');
                throw $exception;
            }
        }
        $this->set('user', $user);
    }
}
