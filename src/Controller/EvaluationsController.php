<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Event\Event;
use App\Model\Model\Evaluations;

class EvaluationsController extends AppController
{
    /**
     * initialization hook method
     * 
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->Evaluation = TableRegistry::get('Evaluation');
        $this->loadComponent('App');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // ajaxリクエストはcsrfチェックをスキップする
        if (in_array($this->request->getParam('action'), ['post'])) {
            $this->getEventManager()->off($this->Csrf);
        }
    }

    /**
     * いいね（評価）登録/削除
     * 
     * @return \Cake\Http\Response
     */
    public function post()
    {
        // レンダリングを無効化
        $this->autoRender = FALSE;
        $this->__save();
    }

    /**
     * いいね保存
     * 
     * @return \Cake\Http\Response
     */
    private function __save()
    {
        if ($this->request->is(['post'])) {
            $params = $this->request->getData();

            // ユーザー情報をリクエストデータとマージ
            $params = $this->App->mergeRequestData(
                $params,
                $this->getAuthUser()->get('id')
            );

            $evaluation = $this->Evaluations->createEvaluationEntity($params);

            try {

            } catch(Exception $e) {
                new \Cake\Http\Exception\InternalErrorException($e);
                return;
            }

            $saveEvaluation = $this->Evaluations->saveEvaluation($evaluation);
            if ($saveEvaluation) {
                echo json_encode(['success' => true, 'evaluationStatus' => $saveEvaluation->status]);
            } else {
                echo json_encode(['success' => false, 'message' => 'エラー：保存に失敗しました']);
            }
        }
    }
}
