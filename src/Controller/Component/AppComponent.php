<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class AppComponent extends Component
{
    /**
     * リクエストデータとユーザー情報をマージする
     * 
     * @param array $requestData リクエストデータ
     * @param int $userIdAry ユーザーID
     * @return array
     */
    public function mergeRequestData(array $requestData, int $userId)
    {
        return array_merge($requestData, $this->convertUserData($userId));
    }

    /**
     * ユーザーデータを変換する
     * 
     * @param int $userId ユーザーID
     * @return array
     */
    // TODO: 共通化する
    private function convertUserData(int $userId)
    {
        return [
            'user_id' => $userId,
        ];
    }
}
