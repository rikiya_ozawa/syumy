<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * AppHelper helper
 */
class AppHelper extends Helper
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

}
