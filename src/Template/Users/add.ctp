<?php
/*
 * ユーザー登録
 *
 */
?>
<?php
    // JSファイル読み込み
    echo $this->Html->script('page/forms/add');
?>
<div class="users-form">
    <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __('ユーザー登録') ?></legend>
            <?= $this->Form->control('name', ['label' => 'ユーザー名']) ?>
            <?= $this->Form->control('email', ['label' => 'メールアドレス']) ?>
            <?= $this->Form->control('password', ['label' => 'パスワード']) ?>
            <?=
                $this->Form->control('password_confirm', [
                    'type' => 'password',
                    'label' => 'パスワード確認用',
                    'required' => true,
                ])
            ?>
    </fieldset>
        <?= $this->Form->button(__('登録'), ['class' => 'btn']); ?>
    <?= $this->Form->end() ?>
</div>
