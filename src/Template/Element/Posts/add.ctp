<?php
/*
 * 投稿登録
 *
 */
?>
<?php
    // JSファイル読み込み
    echo $this->Html->script('page/forms/add');
?>
<div class="posts-form">
    <?= $this->Form->create($post, ['url' => ['action' => 'add']]) ?>
        <fieldset>
            <?= $this->Form->control('content') ?>
    </fieldset>
        <?= $this->Form->button(__('つぶやく'), ['class' => 'btn']); ?>
    <?= $this->Form->end() ?>
</div>
