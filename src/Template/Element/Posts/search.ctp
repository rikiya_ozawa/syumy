<?php
/**
 * 投稿検索
 */
?>
<div class="search-form">
    <?= $this->Form->create($post, ['url' => ['action' => 'search'], 'type' => 'get']); ?>
    <fieldset>
        <?= $this->Form->control('content', ['type'=>'text', 'label' => '']); ?>
        <?= $this->Form->button(__('検索'), ['class' => 'btn']); ?>
    </fieldset>
    <?= $this->Form->end(); ?>
</div>
