<?php
/**
 * 投稿一覧
 */
?>
<?php for ($postIncrement = 0; $postIncrement < count($posts); $postIncrement++): ?>
    <div class="feed-<?= $posts[$postIncrement]->id ?>">
        <table>
            <tbody>
                <tr>
                    <td><?= h($posts[$postIncrement]->user->name) ?></td>
                    <td><?= h($posts[$postIncrement]->user->id) ?></td>
                    <td><?= h($posts[$postIncrement]->content) ?></td>
                    <td><?= h($posts[$postIncrement]->modified) ?></td>
                    <td>
                        <?=
                            $this->element(
                                    'Evaluations/form',
                                    [
                                        'post'              => $posts[$postIncrement],
                                        'evaluationPosts'   => $evaluationPosts[$postIncrement],
                                    ])
                        ?>
                    </td>
                    <td><?= $this->Html->link('編集', '/posts/edit/' . $posts[$postIncrement]->id) ?></td>
                    <td>
                        <?= $this->Form->create($posts[$postIncrement], ['url' => ['action' => 'delete']]) ?>
                            <fieldset>
                                <?= $this->Form->hidden('id') ?>
                            </fieldset>
                        <?= $this->Form->button(__('削除')); ?>
                        <?= $this->Form->end() ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
<?php endfor; ?>