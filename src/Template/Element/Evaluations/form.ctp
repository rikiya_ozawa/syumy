<?php
/**
 * いいねボタン
 * ajax通信を使用
 */
?>
<?php
    use Cake\Core\Configures;

    // JSファイル読み込み
    echo $this->Html->script('page/evaluations/form');
?>
<div class="evaluation">
    <div class="evaluation-count"><?= h(count($post->evaluations)) ?></div>
    <?php if (isset($evaluationPosts->evaluations[0])): ?>
        <input type="button" name="add" value="いいねを取り消す" class="ajax-btn active" id="<?= EVALUATION_STATUS_DELETE ?>">
    <?php else: ?>
        <input type="button" name="add" value="いいね" class="ajax-btn" id="<?= EVALUATION_STATUS_ACTIVE ?>">
    <?php endif; ?>
    <input type="hidden" name="ajax-post" value="<?= $post->id ?>" id="ajax-post">
</div>