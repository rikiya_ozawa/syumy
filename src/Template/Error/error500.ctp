<?php
/**
 * HTTPステータス500番台エラー画面
 */
?>
<?php
    use App\Common\WebCommon;
    use Cake\Core\Configure;
    use Cake\Error\Debugger;
?>
<?php
if (Configure::read('debug')) :
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?php Debugger::dump($error->params) ?>
<?php endif; ?>
<?php if ($error instanceof Error) : ?>
        <strong>Error in: </strong>
        <?= sprintf('%s, line %s', str_replace(ROOT, 'ROOT', $error->getFile()), $error->getLine()) ?>
<?php endif; ?>
<?php
    echo $this->element('auto_table_warning');

    if (extension_loaded('xdebug')) {
        xdebug_print_function_stack();
    }

    $this->end();
endif;
?>
<?php
    $this->App->title(APP_NAME_SUB);

    // エラー表示
    $message = '';
    $errorDescription = '';

    switch ((int)$code) {
        case 500:
            $message = ERR_MSG_500;
            $errorDescription = 'エラーによりサイトが正しく動作していないため、目的のページが表示できません。';
            break;
        default:
            $code = '500';
            $message = ERR_MSG_OTHER;
            $errorDescription = 'お探しのページは、表示することができません。';
            break;
    }
?>
<div id="content_box500">
    <p class="sorry-message">
        <?= $message ?>
    </p>
    <p class="sorry-code">
        <?= $code ?>
    </p>
    <p class="sorry-description">
        <?= $errorDescription ?>
    </p>
    <div class="back500">
        <?= $this->Html->link('TOPに戻る','/posts') ?>
    </div>
</div>