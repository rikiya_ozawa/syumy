<?php
/**
 * HTTPステータス400番台エラー画面
 *
 * @author hoshino.shogo
 */
?>
<?php
    use Cake\Core\Configure;
    use Cake\Error\Debugger;
    use App\Common\WebCommon;
?>
<?php
if (Configure::read('debug')):
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error500.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?php Debugger::dump($error->params) ?>
<?php endif; ?>
<?php if ($error instanceof Error) : ?>
        <strong>Error in: </strong>
        <?= sprintf('%s, line %s', str_replace(ROOT, 'ROOT', $error->getFile()), $error->getLine()) ?>
<?php endif; ?>
<?php
    echo $this->element('auto_table_warning');

    if (extension_loaded('xdebug')):
        xdebug_print_function_stack();
    endif;

    $this->end();
endif;
?>
<?php
    $this->App->title(APP_NAME_SUB);

    // エラー表示
    $message = '';

    switch ((int)$code) {
        case 400:
            $message = $code . ' ' . ERR_MSG_400;
            break;
        case 403:
            $message = $code . ' ' . ERR_MSG_403;
            break;
        case 404:
            $message = $code . ' ' . ERR_MSG_404;
            break;
        default:
            $message = $code . ' ' . ERR_MSG_OTHER;
            break;
    }
?>

<div id="content_box404">
    <p class="sorry">
        <?= $message ?>
    </p>
    <div class="back404">
        <?= $this->Html->link('TOPに戻る','/posts') ?>
    </div>
</div>
