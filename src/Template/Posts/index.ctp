<?php
/**
 * トップページ
 */
?>
<?php
    // 投稿検索
    echo $this->element('Posts/search');
    // 投稿登録
    echo $this->element('Posts/add');
    // 投稿一覧
    echo $this->element('Posts/index');
?>
