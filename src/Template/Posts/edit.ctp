<?php
/*
 * 投稿編集
 *
 */
?>
<?php
    // JSファイル読み込み
    echo $this->Html->script('page/forms/add');
?>
<div class="posts-form">
    <?= $this->Form->create($post, ['url' => ['action' => 'edit']]) ?>
        <fieldset>
            <?= $this->Form->hidden('id'); ?>
            <?= $this->Form->control('content') ?>
    </fieldset>
        <?= $this->Form->button(__('保存'), ['class' => 'btn']); ?>
    <?= $this->Form->end() ?>
</div>
